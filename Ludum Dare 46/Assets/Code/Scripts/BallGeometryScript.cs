﻿using UnityEngine;

public class BallGeometryScript : MonoBehaviour
{
    public BallScript parentScript;

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(name + " collided with " + collision.transform.name);

        CalculateRicochet(collision);

        CollisionInteraction(collision);
    }

    private void CalculateRicochet(Collision collision)
    {
        // Calculates the mean normal of the contact points
        // and tells the parent script to face the direction of the mean normal

        Vector3 collisionMeanNormal = new Vector3(0, 0, 0);

        if (collision.contactCount > 0)
        {
            foreach (ContactPoint contactPoint in collision.contacts)
            {
                collisionMeanNormal += contactPoint.normal;
            }

            collisionMeanNormal /= collision.contactCount;
        }

        // Make sure there is no Y component
        collisionMeanNormal.Set(collisionMeanNormal.x, 0, collisionMeanNormal.z);

        parentScript.ChangeDirection(collisionMeanNormal);
    }

    private void CollisionInteraction(Collision collision)
    {
        if (collision.transform.CompareTag("ShellSection"))
        {
            ShellSectionScript shellSectionScript = collision.transform.GetComponent<ShellSectionScript>();

            shellSectionScript.Hit();
        }
        else if (collision.transform.CompareTag("Player"))
        {
            parentScript.IncreaseSpeed();
        }
    }
}
