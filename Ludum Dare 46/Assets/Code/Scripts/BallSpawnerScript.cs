﻿using UnityEngine;

public class BallSpawnerScript : MonoBehaviour
{
    public GameObject ballPrefab;
    public ObjectPool ballPool;
    public int ballPoolInitialSize;

    public float spawnTimer;
    public float spawnInterval;
    public float firstSpawnTime;

    public float spawnDistance;

    public int activeBallCount;

    public float noBallsGracePeriod;

    private bool isActive;

    private void Awake()
    {
        activeBallCount = 0;

        ballPool.CreatePool(ballPrefab, ballPoolInitialSize);

        spawnTimer = firstSpawnTime;
    }

    private void Start()
    {
        isActive = true;
    }

    private void Update()
    {
        if (isActive)
        {
            SpawnLoop();
        }
    }

    public void IncreaseActiveBallCount()
    {
        ++activeBallCount;
    }

    public void DecreaseActiveBallCount()
    {
        --activeBallCount;

        // Make sure the player does not have to wait for balls
        if (activeBallCount <= 0)
        {
            if (spawnTimer > noBallsGracePeriod)
            {
                spawnTimer = noBallsGracePeriod;
            }
        }
    }

    public void Deactivate()
    {
        isActive = false;
    }

    private void SpawnLoop()
    {
        spawnTimer -= Time.deltaTime;

        if (spawnTimer <= 0)
        {
            SpawnBall();
            spawnTimer += spawnInterval;
        }
    }

    private void SpawnBall()
    {
        GameObject ball = ballPool.GetPooledObject();

        BallScript ballScript = ball.GetComponent<BallScript>();
        ballScript.ballSpawnerScript = this;

        Vector2 spawnCoordinates = Random.insideUnitCircle.normalized * spawnDistance;

        Vector3 spawnPosition = new Vector3(spawnCoordinates.x, 0, spawnCoordinates.y);

        ballScript.Appear(spawnPosition);
    }
}
