﻿using UnityEngine;

public class CoreScript : MonoBehaviour
{
    public GameManager gameManager;

    public float xSizeRange;
    public float ySizeRange;
    public float zSizeRange;

    public float xSizeSpeed;
    public float ySizeSpeed;
    public float zSizeSpeed;

    public float xSizeOffset;
    public float ySizeOffset;
    public float zSizeOffset;

    private void Update()
    {
        ChangeSize();
    }
    
    public void Hit()
    {
        GetComponent<AudioSource>().Play();

        gameManager.GameOver();
    }

    private void ChangeSize()
    {
        float newXScale = xSizeRange * Mathf.Sin(xSizeSpeed * Time.time + xSizeOffset);
        float newYScale = ySizeRange * Mathf.Sin(ySizeSpeed * Time.time + ySizeOffset);
        float newZScale = zSizeRange * Mathf.Sin(zSizeSpeed * Time.time + zSizeOffset);

        transform.localScale = new Vector3(1f + newXScale, 1f + newYScale, 1f + newZScale);
    }
}
