﻿using UnityEngine;

public class GameManager : MonoBehaviour
{
    public ScoreManager scoreManager;

    public GameObject playerObject;
    public GameObject ballSpawner;
    
    public GameObject gameOverScreen;
    public GameObject pauseScreen;

    public bool isPaused;

    private void Awake()
    {
        isPaused = false;
    }

    private void Update()
    {
        KeyListener();
    }

    private void KeyListener()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space))
        {
            if (isPaused)
            {
                ContinueGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void GameOver()
    {
        DeactivateGame();

        gameOverScreen.SetActive(true);
        gameOverScreen.GetComponent<GameOverScreenScript>().UpdateScore(
            scoreManager.GetTimer());

        Debug.Log("You lost the game!");
    }

    private void DeactivateGame()
    {
        //playerObject.SetActive(false);
        playerObject.GetComponent<PaddleScript>().Deactivate();

        //ballSpawner.SetActive(false);
        ballSpawner.GetComponent<BallSpawnerScript>().Deactivate();

        scoreManager.Pause();
    }

    private void PauseGame()
    {
        Time.timeScale = 0;
        isPaused = true;
        pauseScreen.SetActive(true);
    }

    public void ContinueGame()
    {
        Time.timeScale = 1;
        isPaused = false;
        pauseScreen.SetActive(false);
    }
}
