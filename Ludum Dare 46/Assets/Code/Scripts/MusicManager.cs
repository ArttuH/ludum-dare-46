﻿using UnityEngine;
using UnityEngine.Audio;

public class MusicManager : MonoBehaviour
{
    private AudioSource windAudioSource;

    public AudioMixer audioMixer;

    public float attenuationOn;
    public float attenuationOff;

    public float volumeTarget;

    public float volumeSwellSpeed;

    public bool soundIsEnabled;

    private void Awake()
    {
        soundIsEnabled = true;

        DontDestroyOnLoad(gameObject);

        windAudioSource = GetComponent<AudioSource>();

        //if (windAudioSource.volume == 0f)
        //{
            //AmbienceStart();
        //}
    }

    private void Update()
    {
        if (windAudioSource.volume < volumeTarget)
        {
            windAudioSource.volume += Time.deltaTime * volumeSwellSpeed;

            if (windAudioSource.volume > volumeTarget)
            {
                windAudioSource.volume = volumeTarget;
            }
        }
    }

    public void ToggleSound()
    {
        if (soundIsEnabled)
        {
            // Toggle on to off
            audioMixer.SetFloat("masterVolume", attenuationOff);
        }
        else
        {
            // Toggle off to on
            audioMixer.SetFloat("masterVolume", attenuationOn);
        }

        soundIsEnabled = !soundIsEnabled;
    }
}
