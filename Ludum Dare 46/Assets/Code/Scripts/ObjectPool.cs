﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    GameObject pooledObject;

    Queue<GameObject> objectPool;

    public void CreatePool(GameObject pooledObjectPrefab, int initialCount)
    {
        pooledObject = pooledObjectPrefab;

        objectPool = new Queue<GameObject>();

        for (int i = 0; i < initialCount; ++i)
        {
            AddObjectToPool();
        }
    }

    private void AddObjectToPool()
    {
        GameObject instantiatedObject = Instantiate(
                pooledObject, transform.position, transform.rotation) as GameObject;

        PoolableObject poolableObject = instantiatedObject.GetComponent<PoolableObject>();

        if (poolableObject)
        {
            poolableObject.objectPool = this;
        }

        instantiatedObject.SetActive(false);
        instantiatedObject.transform.parent = transform;

        objectPool.Enqueue(instantiatedObject);
    }

    public GameObject GetPooledObject()
    {
        if (objectPool.Count == 0)
        {
            AddObjectToPool();
        }

        GameObject pooledObject = objectPool.Dequeue();

        pooledObject.SetActive(true);

        return pooledObject;
    }

    public void ReturnToPool(GameObject returningObject)
    {
        returningObject.SetActive(false);
        objectPool.Enqueue(returningObject);
    }
}
