﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableObject : MonoBehaviour
{
    public ObjectPool objectPool;

    virtual public void ReturnToPool()
    {
        if (objectPool)
        {
            objectPool.ReturnToPool(gameObject);
        }
    }
}
