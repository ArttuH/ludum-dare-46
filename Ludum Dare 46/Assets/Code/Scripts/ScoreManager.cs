﻿using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public float timer;

    private bool isPaused;

    // Start is called before the first frame update
    void Start()
    {
        isPaused = false;
        timer = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPaused)
        {
            timer += Time.deltaTime;
        }
    }

    public float GetTimer()
    {
        return timer;
    }

    public void Pause()
    {
        isPaused = true;
    }

    public void Unpause()
    {
        isPaused = false;
    }
}
