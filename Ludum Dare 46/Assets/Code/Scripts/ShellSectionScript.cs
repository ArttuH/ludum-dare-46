﻿using UnityEngine;

public class ShellSectionScript : MonoBehaviour
{
    public float burnProgress;

    public float burnTimer;
    public float burnDuration;

    public AnimationCurve burnCurve;

    private MeshRenderer meshRenderer;
    private AudioSource burnAudioSource;

    private bool isBurning;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        burnAudioSource = GetComponent<AudioSource>();

        isBurning = false;
        burnProgress = 0f;
        burnTimer = 0f;
    }

    private void Update()
    {
        if (isBurning)
        {
            Burn();
        }
    }

    public void Hit()
    {
        //Debug.Log(name + " was destroyed!");

        BeginBurning();

        //Destroy(transform.gameObject, burnDuration);
        Destroy(transform.gameObject, burnAudioSource.clip.length);
    }

    private void BeginBurning()
    {
        isBurning = true;

        burnAudioSource.Play();
        GetComponent<Collider>().enabled = false;
    }

    private void Burn()
    {
        if (burnTimer < burnDuration)
        {
            burnTimer += Time.deltaTime;
        }

        if (burnTimer > burnDuration)
        {
            burnTimer = burnDuration;
        }

        burnProgress = burnTimer / burnDuration;

        meshRenderer.material.SetFloat("_Progress", burnCurve.Evaluate(burnProgress));
    }
}
