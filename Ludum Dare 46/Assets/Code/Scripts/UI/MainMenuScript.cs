﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MainMenuScript : MonoBehaviour
{
    public GameObject mainMenuObject;
    public GameObject tutorialObject;
    public GameObject tutorialCoreObject;
    public GameObject tutorialBallObject;
    public GameObject miniCreditsObject;
    public GameObject fullCreditsObject;

    public MusicManager musicManager;
    public NavigationManager navigationManager;

    public TextMeshProUGUI soundToggleText;

    public string soundOnToOffText;
    public string soundOffToOnText;
    
    private void Start()
    {
        if (!musicManager)
        {
            // This is very hacky
            musicManager = GameObject.Find("Ambience(Clone)").GetComponent<MusicManager>();
        }

        if (!navigationManager)
        {
            // This is quite hacky
            navigationManager = GameObject.Find("NavigationManager").GetComponent<NavigationManager>();
        }

        UpdateSoundToggleText();
    }

    public void StartGame()
    {
        navigationManager.isOverUI = false;
        SceneManager.LoadScene("Prototype");
    }

    public void ToggleSound()
    {
        Debug.Log("Toggling sound!");
        musicManager.ToggleSound();

        UpdateSoundToggleText();
    }

    private void UpdateSoundToggleText()
    {
        if (musicManager.soundIsEnabled)
        {
            soundToggleText.text = soundOnToOffText;
        }
        else
        {
            soundToggleText.text = soundOffToOnText;
        }
    }

    public void OpenCredits()
    {
        Debug.Log("Opening credits!");

        mainMenuObject.SetActive(false);
        tutorialObject.SetActive(false);
        tutorialCoreObject.SetActive(false);
        tutorialBallObject.SetActive(false);
        miniCreditsObject.SetActive(false);

        fullCreditsObject.SetActive(true);

        navigationManager.isOverUI = false;
    }

    public void CloseCredits()
    {
        Debug.Log("Closing credits!");

        mainMenuObject.SetActive(true);
        tutorialObject.SetActive(true);
        tutorialCoreObject.SetActive(true);
        tutorialBallObject.SetActive(true);
        miniCreditsObject.SetActive(true);

        fullCreditsObject.SetActive(false);

        navigationManager.isOverUI = false;
    }

    public void QuitGame()
    {
        Debug.Log("Quitting the game!");
        Application.Quit();
    }
}
