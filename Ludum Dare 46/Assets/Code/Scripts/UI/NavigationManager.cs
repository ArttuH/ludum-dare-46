﻿using UnityEngine;
using UnityEngine.EventSystems;

public class NavigationManager : MonoBehaviour
{
    private AudioSource clickAudioSource;

    public float clickPitch;
    public float upPitch;

    public bool isOverUI;

    private void Awake()
    {
        isOverUI = false;

        DontDestroyOnLoad(gameObject);

        clickAudioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (isOverUI)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Click();
            }
            else if (Input.GetMouseButtonUp(0))
            {
                ClickUp();
            }
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        isOverUI = false;
    }

    public void Click()
    {
        clickAudioSource.pitch = clickPitch;
        clickAudioSource.Play();
    }

    public void ClickUp()
    {
        clickAudioSource.pitch = upPitch;
        clickAudioSource.Play();
    }

    public void EnterOverUI()
    {
        isOverUI = true;
    }

    public void ExitOverUI()
    {
        isOverUI = false;
    }
}
